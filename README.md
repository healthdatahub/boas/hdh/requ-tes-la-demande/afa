# AFA

## Titre du projet : 
Prise en charge des patients atteints de la maladie de Crohn et de la rectocolite hémorragique.

## Auteur :
Ana Gabriela Prada (Health Data Hub)

## Objectif de l'algorithme : 
L’algorithme de la requête à la demande, réalisé pour le compte de l’Association François Aupetit (AFA), a pour objectif de cibler et de caractériser la population atteint des maladies inflammatoires chroniques de l’intestin (MICI). Pour ce faire, le programme interroge la base principale du SNDS pour obtenir un état des lieux des MICI et de leur prise en charge en France en 2021. Le programme génère un tableau de bord par type de MICI - maladie de Crohn (K50) et rectocolite hémorragique (K51) - comprenant les éléments suivants :


→ Dénombrement par classe d’âge 

→ Dénombrement par sexe

→ Répartition géographique (département et région)

→ Nombre d’hospitalisations en ambulatoire versus hospitalisations complètes

→ Volume des principales chirurgies digestives 

→ Part des personnes ayant au moins un acte en lien avec une pose de stomie digestive

→ Nombre de jours moyen d’arrêt maladie dans l’année (même s’il n’est pas possible de relier la cause)

→ Recours aux principales classes de médicaments 

→ Description des Affections Longue Durée (ALD) des patients pour identifier les comorbidités associées 

## Méthodologie :
L'algorithme de ciblage des patients atteints de MICI est basé sur celui de la cartographie des pathologies de la CNAM ; les critères sont les suivants :

- Personnes en ALD au cours de l’année n, avec codes CIM-10 de maladie inflammatoire chronique intestinale (maladie de Crohn ou rectocolite hémorragique),
- et/ou personnes hospitalisées en MCO pour ces mêmes motifs durant au moins une des 5 dernières années (Diagnostic principal ou relié),
- et/ou personnes hospitalisées en MCO l’année n pour tout autre motif avec une maladie inflammatoire chronique intestinale comme complication ou morbidité associée (Diagnostic associé, principal ou relié d’un des résumés d’unités médicales),
- et/ou personnes hospitalisées en SSR l’année n avec une maladie inflammatoire chronique intestinale comme manifestation morbide principale (MMP), Affection Étiologique (AE) ou diagnostic associé (DA).

Codes CIM-10 utilisés (PMSI et ALD) :
- K50 (Maladie de Crohn)
- K51 (Recto-colite hémorragique) 

Il existe d'autres algorithmes pour détecter les MICI, cependant, nous avons choisi d'adopter les critères utilisés par la cartographie des pathologies de la CNAM afin de permettre une comparaison des indicateurs. Toutefois, il est envisageable d'adapter le code à d'autres critères selon les besoins spécifiques. 

## Données utilisées : Tables et variables du SNDS nécessaires 

Le programme a été développé sur le portail CNAM en SAS sous accès permanent. 
La mise en œuvre du programme nécessite l’utilisation des tables et variables suivantes :

| Tables | Variables |
| ------ | ------ |
|IR_IMB_R|BEN_NIR_PSA, BEN_RNG_GEM, IMB_ALD_DTD, IMB_ALD_DTF, IMB_ALD_NUM, MED_MTF_COD, INS_DTE, UPD_DTE, IMB_ETM_NAT|
|IR_BEN_R|BEN_NIR_PSA, BEN_RNG_GEM, CTO_IDT_ANO, BEN_CDI_NIR|
|IR_PHA_R|PHA_ATC_CLA, PHA_CIP_C13, PHA_CIP_UCD|
|T_MCOxxB (2017, 2018, 2019, 2020, 2021)|NIR_ANO_17, ETA_NUM, RSA_NUM, EXE_SOI_DTD, DGN_PAL, DGN_REL, GRG_GHM, ASS_DGN, SEJ_NBJ, NBR_SEA|
|T_MCOxxC (2017, 2018, 2019, 2020, 2021)|NIR_ANO_17, ETA_NUM, RSA_NUM, EXE_SOI_DTD, ASS_DGN, DGN_PAL, DGN_REL|
|T_MCO21D|ETA_NUM, RSA_NUM, ASS_DGN|
|T_MCO21UM|ETA_NUM, RSA_NUM, DGN_PAL, DGN_REL|
|T_MCO21A|ETA_NUM, RSA_NUM, CDC_ACT, PHA_ACT, ACV_ACT|
|T_SSR21C|NIR_ANO_17, ETA_NUM, RHA_NUM, EXE_SOI_DTD, MOR_PRP, ETL_AFF, DGN_COD|
|T_SSR21B|NIR_ANO_17, ETA_NUM, RHA_NUM, MOR_PRP, ETL_AFF|
|T_SSR21D|ETA_NUM, RHA_NUM, DGN_COD|
|CRTO_CT_IND_G10_2021|BEN_IDT_ANO, BEN_SEX_COD, BEN_NAI_ANN, AGE, CLA_AGE_CT, CLA_AGE_5, OLD_COD_REG, NEW_COD_REG, DPT,  BEN_DCD_AME, BEN_DCD_DTE|
|ER_PRS_F|BEN_NIR_PSA, EXE_SOI_DTD, EXE_SOI_DTF, RGO_ASU_NAT, PRS_NAT_REF, PRS_ACT_NBR, PRS_ACT_QTE, FLX_DIS_DTD, FLX_TRT_DTD, FLX_EMT_TYP, FLX_EMT_NUM, FLX_EMT_ORD, ORG_CLE_NUM, DCT_ORD_NUM, PRS_ORD_NUM, REM_TYP_AFF, DPN_QLF, IJR_RVL_NAT|
|ER_PHA_F|PHA_PRS_IDE, PHA_PRS_C13, PHA_ACT_PRU, PHA_ACT_QSN, FLX_DIS_DTD, FLX_TRT_DTD, FLX_EMT_TYP, FLX_EMT_NUM, FLX_EMT_ORD, ORG_CLE_NUM, DCT_ORD_NUM,  PRS_ORD_NUM|
|ER_UCD_F|UCD_TOP_UCD, UCD_UCD_COD, UCD_DLV_NBR, UCD_FRC_COE, FLX_DIS_DTD, FLX_TRT_DTD, FLX_EMT_TYP, FLX_EMT_NUM, FLX_EMT_ORD, ORG_CLE_NUM, DCT_ORD_NUM, PRS_ORD_NUM|
|T_MCOxxMED (2017, 2018, 2019, 2020, 2021)|UCD_UCD_COD, ETA_NUM, RSA_NUM|
|T_MCOxxFHSTC (2017, 2018, 2019, 2020, 2021)|UCD_COD, ETA_NUM, SEQ_NUM|
|T_MCOxxCSTC (2017, 2018, 2019, 2020, 2021)|NIR_ANO_17, ETA_NUM, SEQ_NUM|

## Validation :
Non validé (absence de données externes permettant la validation)

## Date de mise à jour :
Juin 2024

## Comment utiliser l’algorithme :
Le programme est à exécuter sous SAS sur le portail CNAM. Il a été développé avec une restitution des données SNDS sous accès permanent. Des adaptations peuvent être nécessaires si le programme doit être exécuté sur une extraction SNDS sur projet.

## Support :
opensource@health-data-hub.fr

## Licence et conditions d’utilisation :
Apache 2.0 


